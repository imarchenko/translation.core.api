﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using TranslationCore.Entities;
using TranslationCore.Entities.Identity;

namespace TranslationCore.Dal
{
    public class MainDBContext : IdentityDbContext<UserAccount, Role, int>
    {
        public MainDBContext(DbContextOptions options) : base(options)
        {
        }

        public virtual DbSet<Language> Languages { get; set; }
        public virtual DbSet<Pronounciation> Pronounciations { get; set; }
        public virtual DbSet<Raiting> Raitings { get; set; }
        public virtual DbSet<RepeatSchedule> RepeatSchedules { get; set; }
        public virtual DbSet<TrainingGroup> TrainingGroups { get; set; }
        public virtual DbSet<TrainingGroupCategory> TrainingGroupCategories { get; set; }
        public virtual DbSet<UserAssociation> UserAssociations { get; set; }
        public virtual DbSet<UserCustomLanguage> UserCustomLanguages { get; set; }
        public virtual DbSet<UserTranslation> UserTranslations { get; set; }
        public virtual DbSet<UserVote> UserVotes { get; set; }
        public virtual DbSet<UserWord> UserWords { get; set; }
        public virtual DbSet<Vote> Votes { get; set; }
        public virtual DbSet<WordInfo> WordInfos { get; set; }
    }
}
