﻿using Microsoft.EntityFrameworkCore.Query;
using System;
using System.Linq;
using System.Linq.Expressions;
using TranslationCore.Entities;

namespace TranslationCore.Dal.Extensions
{
    public static class RepositoryExtensions
    {/// <summary>
     /// Order by property that mentioned in a function. Asceding or desceding should be pointed in the function manually by every call
     /// </summary>
        public static IQueryable<TEntity> OrderBy<TEntity>(this IQueryable<TEntity> query, Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null)
        {
            if (orderBy != null)
            {
                query = orderBy(query);
            }
            return query;
        }

        /// <summary>
        /// Global filters like: !IsDeleted
        /// </summary>
        public static IQueryable<TEntity> GlobalFilter<TEntity>(this IQueryable<TEntity> query, bool useGlobalFilters = true)
            where TEntity : class, IEntity
        {
            return useGlobalFilters ? query.Where(entity => !entity.IsDeleted) : query;
        }

        /// <summary>
        /// Specific filter for specific entity
        /// </summary>
        public static IQueryable<TEntity> Filter<TEntity>(this IQueryable<TEntity> query, Expression<Func<TEntity, bool>> filter = null)
            where TEntity : class, IEntity
        {
            if (filter != null)
            {
                query = query.Where(filter);
            }
            return query;
        }

        /// <summary>
        /// Include entity's properties. Also supports deep levels of including.
        /// </summary>
        public static IQueryable<TEntity> Includes<TEntity>(this IQueryable<TEntity> query, Func<IQueryable<TEntity>, IIncludableQueryable<TEntity, object>> includes)
            where TEntity : class
        {
            if (includes != null)
            {
                query = includes(query);
            }
            return query;
        }
    }
}
