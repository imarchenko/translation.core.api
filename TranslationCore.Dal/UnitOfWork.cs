﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TranslationCore.Dal.Repositories;
using TranslationCore.Entities;

namespace TranslationCore.Dal
{
    public class UnitOfWork
    {
        private readonly MainDBContext _context;
        
        public UnitOfWork(MainDBContext context)
        {
            _context = context;

            LanguageRepository = new BaseRepository<Language>(context);
            PronounciationRepository = new BaseRepository<Pronounciation>(context);
            RaitingRepository = new BaseRepository<Raiting>(context);
            RepeatScheduleRepository = new BaseRepository<RepeatSchedule>(context);
            TrainingGroupRepository = new BaseRepository<TrainingGroup>(context);
            TrainingGroupCategoryRepository = new BaseRepository<TrainingGroupCategory>(context);
            UserAssociationRepository = new BaseRepository<UserAssociation>(context);
            UserCustomLanguageRepository = new BaseRepository<UserCustomLanguage>(context);
            UserTranslationRepository = new BaseRepository<UserTranslation>(context);
            UserVoteRepository = new BaseRepository<UserVote>(context);
            UserWordRepository = new BaseRepository<UserWord>(context);
            VoteRepository = new BaseRepository<Vote>(context);
            WordInfoRepository = new BaseRepository<WordInfo>(context);
        }

        public BaseRepository<Language> LanguageRepository { get; set; }
        public BaseRepository<Pronounciation> PronounciationRepository { get; set; }
        public BaseRepository<Raiting> RaitingRepository { get; set; }
        public BaseRepository<RepeatSchedule> RepeatScheduleRepository { get; set; }
        public BaseRepository<TrainingGroup> TrainingGroupRepository { get; set; }
        public BaseRepository<TrainingGroupCategory> TrainingGroupCategoryRepository { get; set; }
        public BaseRepository<UserAssociation> UserAssociationRepository { get; set; }
        public BaseRepository<UserCustomLanguage> UserCustomLanguageRepository { get; set; }
        public BaseRepository<UserTranslation> UserTranslationRepository { get; set; }
        public BaseRepository<UserVote> UserVoteRepository { get; set; }
        public BaseRepository<UserWord> UserWordRepository { get; set; }
        public BaseRepository<Vote> VoteRepository { get; set; }
        public BaseRepository<WordInfo> WordInfoRepository { get; set; }

        public void Dispose()
        {
            _context.Dispose();
        }
        
        public async Task SaveChangesAsync()
        {
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}
