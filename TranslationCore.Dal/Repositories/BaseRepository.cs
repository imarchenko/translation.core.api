﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using TranslationCore.Dal.Extensions;
using TranslationCore.Entities;

namespace TranslationCore.Dal.Repositories
{
    public class BaseRepository<TEntity> where TEntity : class, IEntity
    {
        protected readonly MainDBContext Context;

        public BaseRepository(MainDBContext context)
        {
            Context = context;
        }

        public async Task AddAsync(TEntity entity)
        {
            await Context.Set<TEntity>().AddAsync(entity);
        }

        public async Task AddRangeAsync(params TEntity[] entities)
        {
            await Context.Set<TEntity>().AddRangeAsync(entities);
        }

        /// <summary>
        /// Physically delete the entity from database
        /// </summary>
        public void DeletePermanently(TEntity entity)
        {
            Context.Set<TEntity>().Remove(entity);
        }

        /// <summary>
        /// Mark as deleted
        /// </summary>
        public void Delete(TEntity entity)
        {
            entity.IsDeleted = true;
            this.Update(entity);
        }

        /// <summary>
        /// Mark as deleted range of entites
        /// </summary>
        public void DeleteRange(IEnumerable<TEntity> entities)
        {
            foreach (var entity in entities)
            {
                this.Delete(entity);
            }
        }

        /// <summary>
        /// Mark as changed
        /// </summary>
        public void Update(TEntity entity)
        {
            Context.Entry(entity).State = EntityState.Modified;
        }

        public async Task SaveChangesAsync()
        {
            await Context.SaveChangesAsync();
        }

        public async Task<TEntity> FindByIdAsync(int id)
        {
            return await Context.Set<TEntity>().FindAsync(id);
        }

        public async Task<TEntity> FirstOrDefaultAsync(
            Expression<Func<TEntity, bool>> predicate,
            Func<IQueryable<TEntity>, IIncludableQueryable<TEntity, object>> includes = null)
        {
            return await this
                .CreateQuery()
                .AsNoTracking()
                .Includes(includes)
                .GlobalFilter()
                .FirstOrDefaultAsync(predicate);
        }

        /// <param name="useGlobalFilters">Use filters such as: !IsDeleted</param>
        public async Task<IEnumerable<TEntity>> GetAllAsync(bool useGlobalFilters = true)
        {
            return await this
                .CreateQuery()
                .GlobalFilter(useGlobalFilters)
                .ToListAsync();
        }

        /// <summary>
        /// Search by filter. If there are no pageNumber and pageSize mentioned, then it will load all elements that will be found
        /// </summary>
        /// <param name="includes">Use Include and ThenInclude methods for including deep levels</param>
        public virtual async Task<IEnumerable<TEntity>> FindByAsync(
          Expression<Func<TEntity, bool>> filter,
          Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
          int? pageNumber = null, int? pageSize = null,
          Func<IQueryable<TEntity>, IIncludableQueryable<TEntity, object>> includes = null)
        {
            var query = this
                 .CreateQuery()
                 .AsNoTracking()
                 .GlobalFilter()
                 .Filter(filter)
                 .Includes(includes)
                 .OrderBy(orderBy);

            if (pageNumber.HasValue && pageSize.HasValue)
                query = query
                    .Skip((pageNumber.Value - 1) * pageSize.Value)
                    .Take(pageSize.Value);

            return await query.ToListAsync();
        }

        /// <summary>
        /// Gets a single entity by predicate or default if there is no such entity. 
        /// Throw an exception if there are more than one entity found
        /// </summary>
        public virtual async Task<TEntity> GetSingleAsync(
           Expression<Func<TEntity, bool>> predicate,
            Func<IQueryable<TEntity>, IIncludableQueryable<TEntity, object>> includes = null)
        {
            return await this
                .CreateQuery()
                .AsNoTracking()
                 .GlobalFilter()
                .Includes(includes)
                .SingleOrDefaultAsync(predicate);
        }

        private IQueryable<TEntity> CreateQuery()
        {
            IQueryable<TEntity> query = Context.Set<TEntity>();
            return query;
        }
    }
}
