﻿namespace TranslationCore.Entities
{
    public abstract class BaseEntity : IEntity
    {
        public virtual int Id { get; protected set; }

        public bool IsDeleted { get; set; }
    }
}
