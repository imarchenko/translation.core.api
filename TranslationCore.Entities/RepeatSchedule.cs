﻿using System;

namespace TranslationCore.Entities
{
    public class RepeatSchedule : BaseEntity
    {
        public const int MinimumHoursToRepeat = 4;
        public const int RepeatProgression = 2;

        public long? LastRepeat { get; protected set; }

        public int SuccessedRepeatsCount { get; protected set; }

        public int FailedRepeatsCount { get; protected set; }

        public int HoursToRepeat { get; protected set; }

        public bool NeedToRepeatNow { get; set; }

        public RepeatSchedule()
        {
            this.HoursToRepeat = MinimumHoursToRepeat;
        }

        public void Update(bool isSucceded)
        {
            if (isSucceded)
            {
                this.SuccessedRepeatsCount++;
                this.HoursToRepeat *= RepeatProgression;
            }
            else
            {
                this.FailedRepeatsCount++;
                if (this.HoursToRepeat > MinimumHoursToRepeat)
                {
                    this.HoursToRepeat /= RepeatProgression;
                }
            }
            this.LastRepeat = DateTime.Now.Ticks;
        }
    }
}
