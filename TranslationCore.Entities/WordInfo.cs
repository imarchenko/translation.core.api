﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace TranslationCore.Entities
{
    public class WordInfo : BaseEntity
    {
        [Required]
        public string Text { get; set; }

        public bool IsLanguageName { get; set; }

        public int? LanguageId { get; set; }
        public Language Language { get; set; }

        public virtual ICollection<Pronounciation> Pronounciations { get; set; }

        public WordInfo()
        {
            Pronounciations = new List<Pronounciation>();
        }
    }
}
