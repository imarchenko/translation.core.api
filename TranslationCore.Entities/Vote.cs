﻿namespace TranslationCore.Entities
{
    public class Vote : BaseEntity
    {
        public int Mark { get; set; }

        public long Date { get; set; }
    }
}
