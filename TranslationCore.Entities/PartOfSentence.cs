﻿namespace TranslationCore.Entities
{
    public enum PartOfSentence
    {
        Unknown,
        Noun,
        Verb
    }
}
