﻿using Microsoft.AspNetCore.Identity;

namespace TranslationCore.Entities.Identity
{
    public class UserAccount : IdentityUser<int>
    {
        public UserAccount()
        {
        }
    }
}
