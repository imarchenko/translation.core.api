﻿namespace TranslationCore.Entities
{
    public class TrainingGroupCategory : BaseEntity
    {
        public string Name { get; set; }

        public int OrderNumber { get; set; }
    }
}
