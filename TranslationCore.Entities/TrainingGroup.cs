﻿using TranslationCore.Entities.Identity;

namespace TranslationCore.Entities
{
    public class TrainingGroup : BaseEntity
    {
        public int CategoryId { get; set; }
        public TrainingGroupCategory Category { get; set; }

        public int? RecordId { get; set; }

        public int OrderNumber { get; set; }

        public int UserId { get; set; }
        public UserAccount User { get; set; }

        public TrainingGroup()
        {
        }
    }
}
