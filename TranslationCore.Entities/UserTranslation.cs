﻿namespace TranslationCore.Entities
{
    public class UserTranslation : BaseEntity
    {
        public int WordInfoId { get; set; }
        public WordInfo WordInfo { get; set; }

        public int UserWordId { get; set; }
        public UserWord UserWord { get; set; }

        public PartOfSentence PartOfSentence { get; set; }

        public int? RaitingId { get; set; }
        public Raiting Raiting { get; set; }

        public int Popularity { get; set; }

        public UserTranslation()
        {
        }
    }
}
