﻿using TranslationCore.Entities.Identity;

namespace TranslationCore.Entities
{
    public class UserAssociation : BaseEntity
    {
        public string CombinedAssociation { get; set; }

        public string ForeignWordAssociation { get; set; }

        public string TranslationAssociation { get; set; }

        public int UserId { get; set; }
        public UserAccount User { get; set; }

        public int? RankId { get; set; }
        public Raiting Rank { get; set; }

        public UserAssociation()
        {
        }

    }
}
