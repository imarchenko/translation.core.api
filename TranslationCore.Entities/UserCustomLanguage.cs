﻿using System.Collections.Generic;
using TranslationCore.Entities.Identity;

namespace TranslationCore.Entities
{
    public class UserCustomLanguage : BaseEntity
    {
        public int UserTranslationId { get; set; }
        public UserTranslation UserTranslation { get; set; }

        public int LanguageId { get; set; }
        public Language Language { get; set; }

        public int UserAccountId { get; set; }
        public UserAccount UserAccount { get; set; }

        public ICollection<UserWord> UserWords { get; set; }

        public UserCustomLanguage()
        {
            UserWords = new List<UserWord>();
        }
    }
}
