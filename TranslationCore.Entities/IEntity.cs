﻿namespace TranslationCore.Entities
{
    public interface IEntity
    {
        int Id { get; }

        bool IsDeleted { get; set; }
    }
}
