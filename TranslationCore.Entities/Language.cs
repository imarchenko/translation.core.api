﻿using System.Collections.Generic;

namespace TranslationCore.Entities
{
    public class Language : BaseEntity
    {
        public virtual ICollection<WordInfo> LanguageNames { get; set; }

        public Language()
        {
            LanguageNames = new List<WordInfo>();
        }
    }
}
