﻿namespace TranslationCore.Entities
{
    public class Pronounciation : BaseEntity
    {
        public int WordId { get; set; }
        public WordInfo Word { get; set; }

        public int? RankId { get; set; }
        public Raiting Rank { get; set; }

        public Pronounciation()
        {
        }
    }
}
