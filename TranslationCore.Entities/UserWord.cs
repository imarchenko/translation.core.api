﻿using System.Collections.Generic;

namespace TranslationCore.Entities
{
    public class UserWord : BaseEntity
    {
        public int? RepeatScheduleId { get; set; }
        public RepeatSchedule RepeatSchedule { get; set; }

        public ICollection<UserTranslation> UserTranslations { get; set; }

        public UserWord()
        {
            UserTranslations = new List<UserTranslation>();
        }
    }
}
