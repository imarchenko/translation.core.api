﻿using System.Collections.Generic;

namespace TranslationCore.Entities
{
    public class Raiting : BaseEntity
    {
        public decimal Avarage { get; set; }

        public int VotesCount { get; set; }

        public ICollection<UserVote> Voutes { get; set; }

        public Raiting()
        {
            Voutes = new List<UserVote>();
        }
    }
}
