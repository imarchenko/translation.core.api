﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using TranslationCore.Services.AccountServices;
using TranslationCore.ViewModels.AccountViewModels;

namespace TranslationCore.API.Controllers
{
    [Route("api/[controller]")]
    public class AccountController : BaseController
    {
        public IAccountService AccountService { get; }

        public AccountController(IAccountService accountService)
        {
            AccountService = accountService;
        }

        [HttpPost]
        [Route("Login")]
        public async Task<IActionResult> LoginAsync(AccountLoginForm form)
        {
            return await base.IfModelStateIsValidAsync(async () =>
            {
                return await this.AccountService.LoginAsync(form);
            });
        }

        [HttpPost]
        [Route("Register")]
        public async Task<IActionResult> RegisterAsync(AccountRegisterForm form)
        {
            return await base.IfModelStateIsValidAsync(async () =>
            {
                return await this.AccountService.RegisterAsync(form);
            });
        }
    }
}