﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using TranslationCore.ViewModels.Results;

namespace TranslationCore.API.Controllers
{
    public class BaseController : Controller
    {
        public delegate ServiceResult ServiceAction();
        public delegate Task<ServiceResult> ServiceActionAsync();

        public IActionResult ServiceResult(ServiceResult result)
        {
            if (result.IsSucceeded)
            {
                return Ok(result.Data);
            }
            return BadRequest(result.Data);
        }

        public IActionResult IfModelStateIsValid(ServiceAction action)
        {
            if (ModelState.IsValid)
            {
                return this.ServiceResult(action());
            }
            return base.BadRequest(ModelState);
        }

        public async Task<IActionResult> IfModelStateIsValidAsync(ServiceActionAsync action)
        {
            if (ModelState.IsValid)
            {
                return this.ServiceResult(await action());
            }
            return base.BadRequest(ModelState);
        }
    }
}
