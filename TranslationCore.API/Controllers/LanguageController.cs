﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using TranslationCore.Services.LanguageServices;
using TranslationCore.ViewModels.LanguageViewModels;

namespace TranslationCore.API.Controllers
{
    [Route("api/[controller]/[action]")]
    public class LanguageController : BaseController
    {
        public ILanguageService LanguageService { get; }

        public LanguageController(ILanguageService languageService)
        {
            LanguageService = languageService;
        }

        [HttpGet]
        [Route("Get")]
        public async Task<IActionResult> GetAsync(int? id = null)
        {
            if (id.HasValue)
            {
                return base.ServiceResult(await this.LanguageService.GetByAsync(lang => lang.Id == id));
            }
            return base.ServiceResult(await this.LanguageService.GetAllAsync());
        }

        [HttpPost]
        [Route("Post")]
        public async Task<IActionResult> PostAsync(LanguageDetailsForm form)
        {
           return await base.IfModelStateIsValidAsync(async () =>
            {
                return await this.LanguageService.AddOrUpdateAsync(form);
            });
        }

        [HttpDelete]
        [Route("Delete")]
        public async Task<IActionResult> DeleteAsync(int id)
        {
            return await base.IfModelStateIsValidAsync(async () =>
            {
                return await this.LanguageService.DeleteAsync(id);
            });
        }
    }
}