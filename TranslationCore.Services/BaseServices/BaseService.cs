﻿using TranslationCore.ViewModels.Results;

namespace TranslationCore.Services.BaseServices
{
    public abstract class BaseService
    {
        public ServiceResult Success<T>(T data)
        {
            return new ServiceResult
            {
                IsSucceeded = true,
                Data = data
            };
        }

        public ServiceResult Success()
        {
            return new ServiceResult
            {
                IsSucceeded = true
            };
        }

        public ServiceResult Fail(string errorMessage)
        {
            return new ServiceResult
            {
                IsSucceeded = false,
                Data = errorMessage
            };
        }
    }
}
