﻿using AutoMapper;
using System;
using System.Linq.Expressions;
using System.Threading.Tasks;
using TranslationCore.Dal.Repositories;
using TranslationCore.Entities;
using TranslationCore.ViewModels.BaseViewModels;
using TranslationCore.ViewModels.Results;

namespace TranslationCore.Services.BaseServices
{
    public abstract class BaseEntityService<TEntity, TRepository, TListItemViewModel, TListViewModel, 
        TDetailsViewModel, TDetailsForm> : BaseService
        where TEntity : class, IEntity, new()
        where TRepository : BaseRepository<TEntity>
        where TListItemViewModel : BaseListItemViewModel
        where TListViewModel : BaseListViewModel<TListItemViewModel>, new()
        where TDetailsViewModel : new()
        where TDetailsForm : IEntityForm
    {
        private TRepository _repository { get; set; }

        public BaseEntityService(TRepository repository)
        {
            _repository = repository;
        }

        public async Task<ServiceResult> AddOrUpdateAsync(TDetailsForm form)
        {
            try
            {
                var entity = form.Id > 0 ? await _repository.FindByIdAsync(form.Id) : new TEntity();
                Mapper.Map(form, entity);
                if (form.Id == 0)
                {
                    await _repository.AddAsync(entity);
                }
                else
                {
                    _repository.Update(entity);
                }
                await _repository.SaveChangesAsync();
                return base.Success(entity.Id);
            }
            catch (Exception ex)
            {
                return base.Fail(ex.Message);
            }
        }

        public async Task<ServiceResult> GetDetailsAsync(int id)
        {
            try
            {
                var detailsViewModel = new TDetailsViewModel();
                var entity = await _repository.FindByIdAsync(id);
                Mapper.Map(entity, detailsViewModel);
                return base.Success(detailsViewModel);
            }
            catch (Exception ex)
            {
                return base.Fail(ex.Message);
            }
        }

        public async Task<ServiceResult> GetAllAsync()
        {
            try
            {
                var listViewModel = new TListViewModel();
                var entities = await _repository.GetAllAsync();
                Mapper.Map(entities, listViewModel);
                return base.Success(listViewModel);
            }
            catch (Exception ex)
            {
                return base.Fail(ex.Message);
            }
        }

        public async Task<ServiceResult> GetByAsync(Expression<Func<TEntity, bool>> expression)
        {
            try
            {
                var listViewModel = new TListViewModel();
                var entities = await _repository.FindByAsync(expression);
                Mapper.Map(entities, listViewModel);
                return base.Success(listViewModel);
            }
            catch (Exception ex)
            {
                return base.Fail(ex.Message);
            }
        }

        public async Task<ServiceResult> DeleteAsync(int id)
        {
            try
            {
                var entity = await _repository.FindByIdAsync(id);
                _repository.Delete(entity);
                await _repository.SaveChangesAsync();
                return base.Success();
            }
            catch (Exception ex)
            {
                return base.Fail(ex.Message);
            }
        }
    }
}
