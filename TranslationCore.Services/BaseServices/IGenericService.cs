﻿using System;
using System.Linq.Expressions;
using System.Threading.Tasks;
using TranslationCore.ViewModels.Results;

namespace TranslationCore.Services.BaseServices
{
    public interface IGenericService<TEntity, TDetailsForm>
    {
        Task<ServiceResult> GetDetailsAsync(int id);

        Task<ServiceResult> GetAllAsync();

        Task<ServiceResult> GetByAsync(Expression<Func<TEntity, bool>> expression);

        Task<ServiceResult> AddOrUpdateAsync(TDetailsForm form);

        Task<ServiceResult> DeleteAsync(int id);
    }
}
