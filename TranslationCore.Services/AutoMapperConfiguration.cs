﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using TranslationCore.Entities.Identity;
using TranslationCore.ViewModels.AccountViewModels;

namespace TranslationCore.Services
{
    public static class AutoMapperConfiguration
    {
        public static void RegisterMapping()
        {
            Mapper.Initialize(config =>
            {
                Account(config);
            });
        }

        private static void Account(IMapperConfigurationExpression config)
        {
            config.CreateMap<AccountRegisterForm, UserAccount>()
                .ForMember(d => d.UserName, c => c.MapFrom(s => s.Email));
        }
    }
}
