﻿using System.Threading.Tasks;
using TranslationCore.ViewModels.AccountViewModels;
using TranslationCore.ViewModels.Results;

namespace TranslationCore.Services.AccountServices
{
    public interface IAccountService
    {
        Task<ServiceResult> LoginAsync(AccountLoginForm form);

        Task<ServiceResult> RegisterAsync(AccountRegisterForm form);

        Task<ServiceResult> ChangePasswordAsync(AccountChangePasswordForm form);

        Task<ServiceResult> ResetPasswordAsync(AccountResetPasswordForm form);

        Task<ServiceResult> VerifyResetPasswordTokenAsync(AccountVerifyResetPasswordTokenForm form);

        Task<ServiceResult> DeactivateAccountAsync(AccountDeactivateForm form);
    }
}
