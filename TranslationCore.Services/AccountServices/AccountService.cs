﻿using AutoMapper;
using Microsoft.AspNetCore.Identity;
using System.Linq;
using System.Threading.Tasks;
using TranslationCore.Entities.Identity;
using TranslationCore.Services.BaseServices;
using TranslationCore.ViewModels.AccountViewModels;
using TranslationCore.ViewModels.Results;

namespace TranslationCore.Services.AccountServices
{
    public class AccountService : BaseService, IAccountService
    {
        public SignInManager<UserAccount> SignInManager { get; }
        public UserManager<UserAccount> UserManager { get; }

        public AccountService(
            SignInManager<UserAccount> signInManager,
            UserManager<UserAccount> userManager)
        {
            SignInManager = signInManager;
            UserManager = userManager;
        }
        
        public async Task<ServiceResult> LoginAsync(AccountLoginForm form)
        {
            var signInResult = await this.SignInManager.PasswordSignInAsync
                (userName: form.Email, password: form.Password, isPersistent: true, lockoutOnFailure: true);
            if (!signInResult.Succeeded)
            {
                return base.Fail("Invalid email or password");
            }
            else if (signInResult.IsLockedOut || signInResult.IsNotAllowed)
            {
                return base.Fail("User can't sign in");
            }
            return base.Success();  
        }

        public async Task<ServiceResult> RegisterAsync(AccountRegisterForm form)
        {
            var user = Mapper.Map(form, new UserAccount());
            var identityResult = await this.UserManager.CreateAsync(user, form.Password);
            if (identityResult.Succeeded)
            {
                return base.Success();
            }
            return base.Fail(identityResult.Errors.First().Description);
        }

        public Task<ServiceResult> ChangePasswordAsync(AccountChangePasswordForm form)
        {
            throw new System.NotImplementedException();
        }

        public Task<ServiceResult> ResetPasswordAsync(AccountResetPasswordForm form)
        {
            throw new System.NotImplementedException();
        }

        public Task<ServiceResult> VerifyResetPasswordTokenAsync(AccountVerifyResetPasswordTokenForm form)
        {
            throw new System.NotImplementedException();
        }

        public Task<ServiceResult> DeactivateAccountAsync(AccountDeactivateForm form)
        {
            throw new System.NotImplementedException();
        }
    }
}
