﻿using TranslationCore.Entities;
using TranslationCore.Services.BaseServices;
using TranslationCore.ViewModels.LanguageViewModels;

namespace TranslationCore.Services.LanguageServices
{
    public interface ILanguageService : IGenericService<Language, LanguageDetailsForm>
    {
        
    }
}
