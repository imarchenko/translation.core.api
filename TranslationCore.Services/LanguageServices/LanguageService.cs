﻿using TranslationCore.Dal.Repositories;
using TranslationCore.Entities;
using TranslationCore.Services.BaseServices;
using TranslationCore.ViewModels.LanguageViewModels;

namespace TranslationCore.Services.LanguageServices
{
    public class LanguageService : BaseEntityService<Language, BaseRepository<Language>,
        LanguageListItemViewModel, LanguageListViewModel,
        LanguageDetailsViewModel, LanguageDetailsForm>, ILanguageService
    {
        public LanguageService(BaseRepository<Language> repository) : base(repository)
        {
        }
    }
}
