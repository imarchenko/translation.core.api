﻿using System.ComponentModel.DataAnnotations;

namespace TranslationCore.ViewModels.AccountViewModels
{
    public class AccountVerifyResetPasswordTokenForm
    {
        [Required]
        [MaxLength(Validations.Token_MaxLength)]
        public string Token { get; set; }
    }
}
