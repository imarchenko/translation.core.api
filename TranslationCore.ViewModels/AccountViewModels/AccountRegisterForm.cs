﻿using System.ComponentModel.DataAnnotations;

namespace TranslationCore.ViewModels.AccountViewModels
{
    public class AccountRegisterForm
    {
        [Required]
        [EmailAddress]
        [MaxLength(Validations.TextField_MaxLength)]
        public string Email { get; set; }

        [Required]
        [MaxLength(Validations.TextField_MaxLength)]
        public string Password { get; set; }
    }
}
