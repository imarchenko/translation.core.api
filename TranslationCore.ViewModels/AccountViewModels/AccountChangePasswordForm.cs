﻿using System.ComponentModel.DataAnnotations;

namespace TranslationCore.ViewModels.AccountViewModels
{
    public class AccountChangePasswordForm
    {
        [Required]
        [MaxLength(Validations.TextField_MaxLength)]
        public string OldPassword { get; set; }

        [Required]
        [MaxLength(Validations.TextField_MaxLength)]
        public string NewPassword { get; set; }
    }
}
