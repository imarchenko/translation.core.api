﻿using System.ComponentModel.DataAnnotations;

namespace TranslationCore.ViewModels.AccountViewModels
{
    public class AccountResetPasswordForm
    {
        [Required]
        [EmailAddress]
        [MaxLength(Validations.TextField_MaxLength)]
        public string Email { get; set; }
    }
}
