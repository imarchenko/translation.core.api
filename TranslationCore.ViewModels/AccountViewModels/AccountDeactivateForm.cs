﻿using System.ComponentModel.DataAnnotations;

namespace TranslationCore.ViewModels.AccountViewModels
{
    public class AccountDeactivateForm
    {
        [Required]
        [MaxLength(Validations.TextField_MaxLength)]
        public string Password { get; set; }
    }
}
