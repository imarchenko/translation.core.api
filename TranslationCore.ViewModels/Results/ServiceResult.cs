﻿namespace TranslationCore.ViewModels.Results
{
    public class ServiceResult
    {
        public bool IsSucceeded { get; set; }

        public object Data { get; set; }
        
    }
}
