﻿namespace TranslationCore.ViewModels
{
    public static class Validations
    {
        public const int TextField_MaxLength = 100;
        public const int Token_MaxLength = 256;
    }
}
