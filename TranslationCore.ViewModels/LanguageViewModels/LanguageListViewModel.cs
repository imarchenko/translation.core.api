﻿using TranslationCore.ViewModels.BaseViewModels;

namespace TranslationCore.ViewModels.LanguageViewModels
{
    public class LanguageListViewModel : BaseListViewModel<LanguageListItemViewModel>
    {
    }
}
