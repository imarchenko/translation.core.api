﻿using TranslationCore.ViewModels.BaseViewModels;

namespace TranslationCore.ViewModels.LanguageViewModels
{
    public class LanguageDetailsForm : EntityForm
    {
        public string Name { get; set; }
    }
}
