﻿namespace TranslationCore.ViewModels.BaseViewModels
{
    public interface IEntityForm
    {
        int Id { get; }
    }
}
