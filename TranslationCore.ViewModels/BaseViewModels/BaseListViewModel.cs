﻿using System.Collections.Generic;

namespace TranslationCore.ViewModels.BaseViewModels
{
    public class BaseListViewModel<TListItemViewModel>
        where TListItemViewModel : BaseListItemViewModel
    {
        public List<TListItemViewModel> List { get; set; }

        public BaseListViewModel()
        {
            this.List = new List<TListItemViewModel>();
        }
    }
}
